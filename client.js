let ws = new WebSocket('ws://localhost:2121');

const sendButton = document.querySelector('#send');
const messages = document.querySelector('#messages');
const inputBox = document.querySelector('#inputBox');

function showMessage(message) {
  messages.textContent += `\n\n${message}`;
  messages.scrollTop = messages.scrollHeight;
  inputBox.value = '';
}

ws.onopen = () => {
  console.log('Connection opened');
  ws.send('new connection');
}

ws.onmessage = (message) => {
  showMessage(message.data)
};

sendButton.onclick = function() {
  if (!ws) {
    showMessage("No WebSocket connection");
    return;
  }

  ws.send(inputBox.value);
}

ws.onclose = function() {
  ws = null;
}