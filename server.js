const WebSocket = require('ws');
const qAndA = require('./q-and-a.json')

const websocketserver = new WebSocket.Server({ port: 2121 });

let questions = qAndA.map((item) => {
  return item.question
})

websocketserver.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {   
    websocketserver.clients.forEach(function each(client) {
      if (client.readyState === WebSocket.OPEN) {
        if (message === 'new connection') {
          client.send('----New user connection----')
        } else {
          client.send(`user: ${message}`);

          questions.forEach((question, index) => {
            if (message.toLowerCase() === question) {
              client.send(`chatBot: ${qAndA[index].answer}`)
            }
          })
        }
      }
    });
  })
})

